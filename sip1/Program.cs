﻿using System;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using Word = Microsoft.Office.Interop.Word;

namespace sip1
{
    public partial class Program
    {
        static int N = 2, M = 1;
        static string[] names;
        static string[,] mas;
        static string[] triads;
        public static Excel.Worksheet sheet1;
        public static Excel.Application app;
        static string path, path1;
        public static Excel.Workbook wb;
        private static string font;

        private static Word.Application appw;
        private static Word.Document doc;
        static Random r = new Random();

        private static void GenTriads()
        {
            bool[] isUsing = new bool[M];
            int b, i = 0;
            triads = new string[3];
            while (i < triads.Length)
            {
                if (!isUsing[b = r.Next(1, M)])
                {
                    triads[i] = mas[b, r.Next(2, N)];
                    isUsing[b] = true;
                    i++;
                }
            }
        }

        private static void SaveToWord()
        {
            appw = new Word.Application();
            Object templatePathObj = path;
            Object missingObj = System.Reflection.Missing.Value;
            Object trueObj = true;
            Object falseObj = false;

            try
            {
                doc = appw.Documents.Add(ref templatePathObj, ref missingObj, ref missingObj, ref missingObj);
            }
            catch (Exception e)
            {
                doc.Close(ref falseObj, ref missingObj, ref missingObj);
                appw.Quit(ref missingObj, ref missingObj, ref missingObj);
                doc = null;
                appw = null;
                throw e;
            }
            string str;
            for (int j = 2; j < names.Length; j++)
            {
                str = "";
                doc.Bookmarks["name"].Range.Text = names[j];
                for (int i = 0; i < triads.Length; i++)
                {
                    str += triads[i];
                    if (i != triads.Length - 1)
                        str += ", ";
                }
                GenTriads();
                doc.Bookmarks["wish"].Range.Text = str;

                if (j != names.Length - 1)
                {
                    doc.Words.Last.InsertBreak(Word.WdBreakType.wdPageBreak);
                    doc.Words.Last.InsertFile(path);
                }
            }
            Word.Range rng = doc.Range(doc.Content.Start, doc.Content.End);
            rng.Font.Name = font;
            if (!Directory.Exists(
                path1))
                Directory.CreateDirectory(path1);
            doc.SaveAs2(path1 + Directory.GetFiles(path1).Length.ToString());
            doc.Close();
            appw.Quit();
        }
        private static void Get_NandM()
        {
            int n = 2, m = 1;
            while (sheet1.Range[$"{Convert.ToChar(m + 64)}{n}"].Value2 != null)
            {
                while (sheet1.Range[$"{Convert.ToChar(m + 64)}{n}"].Value2 != null)
                {
                    n++;
                }
                N = n;
                n = 2;
                m++;
            }
            M = m;
        }

        static void Main(string[] args)
        {
            app = new Excel.Application();
            wb = app.Workbooks.Open(Path.GetFullPath("config.xlsx"));
            sheet1 = app.Worksheets[1];
            Get_NandM();
            names = new string[N];
            for (int i = 2; i < N; i++)
            {
                names[i] = Convert.ToString(sheet1.Range[$"A{i}"].Value2);
            }
            sheet1 = app.Worksheets[2];
            Get_NandM();
            if (M - 1 < 3 || Math.Pow(N, M) < names.Length)
                Console.WriteLine("Невозможно сгенерировать нужное количество триад!");
            else
            {
                mas = new string[M, N];
                for (int j = 1; j < M; j++)
                {
                    for (int i = 2; i < N; i++)
                    {
                        mas[j, i] = Convert.ToString(sheet1.Range[$"{Convert.ToChar(j + 64)}{i}"].Value2);
                    }
                }
                sheet1 = app.Worksheets[3];
                path = Convert.ToString(sheet1.Range["A2"].Value2);
                path1 = Convert.ToString(sheet1.Range["A3"].Value2);
                font = Convert.ToString(sheet1.Range["A4"].Value2);

                GenTriads();
                SaveToWord();
                Console.WriteLine("Готово!");
                Console.ReadKey();

            }
            wb.Close();
            app.Quit();
        }
    }
}
